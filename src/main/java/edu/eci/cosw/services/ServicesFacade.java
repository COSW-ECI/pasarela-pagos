/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.cosw.services;

import edu.eci.cosw.samples.model.PaymentDescriptor;
import edu.eci.cosw.samples.model.Transaction;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author hcadavid
 */
@Service
public class ServicesFacade {
   
    //Numero tarjeta, saldo
    private static final Hashtable<String,Integer> amexCards=new Hashtable<>();
    private static final Hashtable<String,Integer> masterCards=new Hashtable<>();
    private static final Hashtable<String,Integer> visaCards=new Hashtable<>();
    private static final LinkedList<PaymentDescriptor> transactions=new LinkedList<>();
    private static final Random rnd=new Random(System.currentTimeMillis());
    
    
    static{
        amexCards.put("378282246310005", 2000000);
        masterCards.put("5105105105105100", 2342343);
        visaCards.put("4111111111111111", 0);
        transactions.add(new PaymentDescriptor("378282246310005","221","AMEX","Juan Perez","2789817823-bancolombia","Ejemplo",10000));
    }
    
    
    private static final Logger LOG = Logger.getLogger(ServicesFacade.class.getName());
    
    
    public List<PaymentDescriptor> getTransactions(){
        return transactions;
    }
    
    public int doPayment(PaymentDescriptor pd) throws NoFundsException,InvalidCardException{
        if (pd.getTipo().equals("AMEX")){
            if (amexCards.containsKey(pd.getNumeroTarjeta())){
                if (amexCards.get(pd.getNumeroTarjeta()) >= pd.getMontoTransaccion()){
                    LOG.info("Pago realizado:"+pd.getNumeroTarjeta()+" por $"+pd.getMontoTransaccion());
                    transactions.add(pd);
                    return Math.abs(rnd.nextInt());
                }
                else{
                    throw new NoFundsException("Saldo insuficiente para la tarjeta "+pd.getNumeroTarjeta());
                }
            }
            else{
                throw new InvalidCardException("Tarjeta inváliad:"+pd.getNumeroTarjeta());
            }
        }
        else if (pd.getTipo().equals("VISA")){
            if (visaCards.containsKey(pd.getNumeroTarjeta())){
                if (visaCards.get(pd.getNumeroTarjeta()) >= pd.getMontoTransaccion()){
                    LOG.info("Pago realizado:"+pd.getNumeroTarjeta()+" por $"+pd.getMontoTransaccion());
                    transactions.add(pd);
                    return Math.abs(rnd.nextInt());
                }
                else{
                    throw new NoFundsException("Saldo insuficiente para la tarjeta "+pd.getNumeroTarjeta());
                }
            }
            else{
                throw new InvalidCardException("Tarjeta inváliada:"+pd.getNumeroTarjeta());
            }
        }
        else if (pd.getTipo().equals("MASTER")){
            if (masterCards.containsKey(pd.getNumeroTarjeta())){
                if (masterCards.get(pd.getNumeroTarjeta()) >= pd.getMontoTransaccion()){
                    LOG.info("Pago realizado:"+pd.getNumeroTarjeta()+" por $"+pd.getMontoTransaccion());
                    transactions.add(pd);
                    return Math.abs(rnd.nextInt());
                }
                else{
                    throw new NoFundsException("Saldo insuficiente para la tarjeta "+pd.getNumeroTarjeta());
                }
            }
            else{
                throw new InvalidCardException("Tarjeta inváliad:"+pd.getNumeroTarjeta());
            }
        }

        
        else{
            throw new InvalidCardException("Tarjeta no soportada:"+pd.getTipo()+". Tipos aceptados: AMEX, VISA y MASTERCARD");
        }
        
                            
        
    }
    
    
}
