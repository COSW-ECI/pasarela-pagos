/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.cosw.controllers;

import edu.eci.cosw.samples.model.PaymentDescriptor;
import edu.eci.cosw.samples.model.Transaction;
import edu.eci.cosw.services.CardTypes;
import edu.eci.cosw.services.InvalidCardException;
import edu.eci.cosw.services.NoFundsException;
import edu.eci.cosw.services.ServicesFacade;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author hcadavid
 */
@RestController
@RequestMapping("/payments")
public class PaymentsController {
    
    @Autowired
    ServicesFacade services;
    
    
    @RequestMapping(method = RequestMethod.GET)        
    public String check() {
        return "REST API OK";        
    }
    
    @RequestMapping(method = RequestMethod.POST)        
    public ResponseEntity<?> addProduct(@RequestBody PaymentDescriptor p) {       
        try{            
            int transCode=services.doPayment(p);
            return new ResponseEntity<>(transCode,HttpStatus.ACCEPTED);
        }
        catch(InvalidCardException e){
            return new ResponseEntity<>("Tarjeta de crédito inválida:"+e.getLocalizedMessage(),HttpStatus.BAD_REQUEST);            
        }
        catch(NoFundsException e){
           return new ResponseEntity<>("Tarjeta de crédito con fondos insuficientes:"+e.getLocalizedMessage(),HttpStatus.BAD_REQUEST);             
        }
        
    }

    
    @RequestMapping(value = "/transactions",method = RequestMethod.GET)        
    public List<PaymentDescriptor> allTransactions() {        
        return services.getTransactions();
    }
    

    
    
}

