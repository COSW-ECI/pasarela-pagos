

*Consulta transacciones realizadas:
GET  http://paymentsgateway.herokuapp.com/rest/payments/transactions

*Realización de pagos:
POST con un objeto jSON:
	{"numeroTarjeta":"XXXXXXXXXXXXXXX",
	 "codigoSeguridad":"XXX",
	 "tipo":"AMEX",
	 "nombreCliente":"aaaaaaaa",
	 "cuentaDestino":"2789817823-bancolombia",
	 "descripcion":"Descripcion del pago",
	 "montoTransaccion":10000}

Al recurso:
http://paymentsgateway.herokuapp.com/rest/payments/transactions


Datos disponibles para pruebas:


Tipo: AMEX
Número:378282246310005
Saldo:2000000
Pin seguridad: cualquiera

Tipo: MASTER
Número:5105105105105100
Saldo:2342343
Pin seguridad: cualquiera

Tipo: VISA
Número:4111111111111111
Saldo:0
Pin seguridad: cualquiera
